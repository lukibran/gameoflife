#################################
#        Lukas Branigan 	#
#           gs13m011		#
#Personenkennzeichen: 1310585011# 
#################################
					
                                        ############
                                        #GameOfLife#
 				        ############
				     

#Aufbau:

Das Progamm Besteht aus 3 Klassen Gol, Grid und Timer.

Gol: Besteht aus Methoden zum 
	-Einlesen der Griddaten aus einem File (readfile)
	-Speichern der Griddaten in ein File (savefile)

Grid: Besteht aus Methoden zum 
	-Bekommen der Griddaten aus der Gol Klasse (getboard)
	-Setzen des ver�nderten Boards (setboard)
	-Durchlaufen des Algorithmus (algo)
	-Erweitern des Grid um einen kompletten Rahmen (add)
	-L�schen dieses Rahmens (remove)

	
#Funktionsweise des Programms/Algorithmus:

-Zuerst werden Daten die Daten aus den Testfiles in ein Zweidimensionales Array geschrieben.
-Es werden au�erdem die Abmessungen des Grid in 2 Variablen geschrieben
-Dieses Array, Abmessungen und die Anzahl der Generationen werden mittels der Methode getboard and 
die Grid Klasse �bergeben
-In dieser Klasse werden 2 weitere Zweidimensionale Arrays erstellt eines mit der originalen Gr��e und 
eines erweitert um einen kompletten Ring
-In der Methode algo wird eine Methode add aufgeruden welche das erweiterte Array mit den Daten in der 
Mitte bef�llt au�erdem wird die untere Zeile nach oben, die linke Zeile nach Rechts, die obere Zeile nach Unten +
und die Rechte Spalte nach Links kopiert. Am Schluss werden noch die Eckpunkte in die jeweils andere Ecke kopiert.
-In der Methode Algo werden 9 Pointer gesetzt welche innherhalb von zwei For-Schleifen immer bis zum Ende der Zeile
verschoben und dann wieder eine Zeile nach unten an den Anfang gesetzt werden. Mittels einer Variable life 
kann �berpr�ft werden wieviele der umliegenden Zellen am Leben sind.
-Tritt eine Ver�nderung einer Zelle auf wird dies wieder in das kleinere Zweidimensionale Array hineingeschrieben.
-Dies wird solange wiederholt bis alle Generationen durchlaufen wurden.
-Das fertige Array kann dann mittels setboard in die Klasse Gol verschoben werden wo dieses in ein Output File 
gespeichert wird.

-Am Schluss kommt die Timer Klasse noch zum Einsatz, welcher eine Funktion printTime hinzugef�gt wurde. Diese
gibt wenn aufgerufen mittels des Timer Objekts die Zeit auf stdout aus.
-Gemessen wurde 
	-die Zeit der Erstellung und Bef�llung des Grid in der Methode readfile (Gol)
	-die Zeit welcher der Algorithmus zur durchf�hrung der Operationen ben�tigt �ber die Methoden
	getboard, algo, add, remove und setboard (Grid)
	-die Zeit f�r das Schreiben der Griddaten in ein File und die Deallokation der Arrays im main 
	�ber die Funktion savefile





