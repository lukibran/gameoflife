#include "Gol.h"
#include "Grid.h"
#include "Timer.h"


using namespace std;

int main(int argc, char * argv[])
{
	int gen = 0;
	bool measure=false;
	Gol mGol;
	char *fname = 0;
	char* sname = 0;
	Timer timer;

	/*
	//Testf�lle
	char* fname = ("test.txt");
	char* sname = ("test1.txt");
	mGol.readfile(fname,gen);
	mGol.savefile(sname);
	*/

	//Durchparsen der Commandline Argumente und je nach dem Aufrufen der Funktionen 
	if(argc < 6 && argc > 8)
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }

    if(strcmp(("--load"),argv[1])==0)
    {
	    fname = argv[2];
    }
    else
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }
                       
    if(strcmp(("--save"),argv[3])==0)
    {
		sname = argv[4];
		
	}
    else
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }

    if(strcmp(("--generations"),argv[5])==0)
    {
		gen = atoi(argv[6]);
		if(gen == 0)
		{
			cout << "Generations is 0! Generations min 1!\n";
            cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
        }
    }
    else
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }
      
	if(argc >= 8)
    {
		if(strcmp(("--measure"),argv[7])==0)
        {
			measure = true;
        }
    }
	//Aufrufen der Readfile Funktion und �bergeben der Parameter
	mGol.readfile(fname,gen,measure);
	
	//Savefile Funktion und Zeitmessung
	if(measure==true)
		timer.start();

	mGol.savefile(sname);

	if(measure == true)
	{
		timer.stop();
		timer.printTime(timer);
	}
	
    return 0;
}