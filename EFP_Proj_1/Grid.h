#ifndef Grid_H
#define Grid_H

#include <iostream>
#include "Timer.h"


class Grid
{
public:
	Grid(void);
	~Grid(void);
	void getboard(int height, int width, int gen, bool gmeasure, char** board);
	void setboard(char** gboard);
	void algo();
	void add();
	void remove();
	
	char** gboard;

private:
	int gheight,gwidth,ggen;
	int cheight,cwidth;
	char** cboard;
	char* topleft,*topmid,*topright;
	char* midleft,*center,*midright;
	char* botleft,*botmid,*botright;
	int life;
	Timer ctimer;
	bool cmeasure;
	int n_gen;
	int msec,h,min,sec;
	
	
};

#endif
