#include "Grid.h"

using namespace std;

Grid::Grid(void)
{
	char** gboard = NULL;
	char** cboard = NULL;
	int gheight = 0;
	int gwidth = 0;
	int ggen = 0;
	int cheight = 0;
	int cwidth = 0;
	int life = 0;
	bool cmeasure = false;
	

	char* topleft = 0;
	char* topmid = 0;
	char* topright = 0;
	char* midleft = 0;
	char* center = 0;
	char* midright = 0;
	char* botleft = 0;
	char* botmid = 0;
	char* botright = 0;
	
}


Grid::~Grid(void)
{
	//L�schen des erweiterten Boards
	for(int i=0;i<cheight;i++)
	{
		delete[] cboard[i];
	}
	delete [] cboard;

	//L�schen des normalen Boards
	for(int i=0;i<gheight;i++)
	{
		delete[] gboard[i];
	}
	delete [] gboard;
}


void Grid::getboard(int height, int width, int gen, bool gmeasure, char** board)
{

	gheight = height;
	gwidth = width;
	ggen = gen;
	cmeasure = gmeasure;

	//Zeitmessung f�r Haupt Algorithmus starten
	if(cmeasure == true)
		ctimer.start();

	gboard = new char *[gheight];
	for(int i=0;i<gheight;i++)
	{
		gboard[i] = new char[gwidth];
	}
	
	gboard = board;

	//Board dynamisch erstellen um je eine Reihe gr��er auf jeder Seite 
	//um es leichter beim Algorithmus zu haben
	cwidth = gwidth + 2;
	cheight = gheight + 2;

	cboard = new char *[cheight];
	for(int i=0;i<cheight;i++)
	{
		cboard[i] = new char[cwidth];
	}
	
	//Algo aufrufen um die Berechnungen zu machen
	algo();
}

void Grid::setboard(char** board)
{
	board = gboard;
}

void Grid::add()
{
	//Rechts unten nach links oben schreiben
	cboard[0][0] = gboard[gheight-1][gwidth-1];

	//Unterste Zeile nach oben schreiben
	for(int i=1;i<=gwidth;i++)
	{
		cboard[0][i] = gboard[gheight-1][i-1];
	}

	//Links unten nach rechts oben schreiben
	cboard[0][cwidth-1] = gboard[gheight-1][0];

	//Rechte Spalte in linke Spalte schreiben
	for(int i=1;i<=gheight;i++)
	{
		cboard[i][0] = gboard[i-1][gwidth-1];
	}

	//Board nach Calcboard schreiben
	for(int i=1; i<=gheight; i++)
	{
		for(int j=1; j<=gwidth; j++)
		{
			cboard[i][j] = gboard[i-1][j-1];
		}			
	}
	
	//Linke Spalte in Rechte Spalte schreiben
	for(int i=1;i<=gheight;i++)
	{
		cboard[i][cwidth-1] = gboard[i-1][0];
	}

	//Rechts oben nach links Unten schreiben
	cboard[cheight-1][0] = gboard[0][gwidth-1];

	//Obere Zeile nach unten schreiben
	for(int i=1;i<=gwidth;i++)
	{
		cboard[cheight-1][i] = gboard[0][i-1];
	}

	//Links oben nach rechts unten verschieben
	cboard[cheight-1][cwidth-1] = gboard[0][0];
	
}

void Grid::algo()
{
	//Schleife f�r die einzelnen Generationen
	for(int i=0;i<ggen;i++)
	{
		//Hier wird die add Funktion aufgerufen um das vergr��erte Array zu bauen
		add();

		//Testausgabe
	/*	for(int a=0; a<cheight; a++)
		{
			cout << endl;
			for(int b=0; b<cwidth; b++)
			{
				cout << cboard[a][b];
			}
		}*/

		//Durchlaufen des gesamten Arrays und �berpr�fen auf Center Zelle
		for(int j=0;j<gheight;j++)
		{
			//Pointer auf Richtige Position setzen 
			topleft = &cboard[j][0];
			topmid = &cboard[j][1];
			topright = &cboard[j][2];
			midleft = &cboard[j+1][0];
			center = &cboard[j+1][1];
			midright = &cboard[j+1][2];
			botleft = &cboard[j+2][0];
			botmid = &cboard[j+2][1];
			botright = &cboard[j+2][2];

			for(int k=0;k<gwidth;k++)
			{
				//Z�hlen der Toten und Lebendigen Zellen und Weiterschieben der Pointer
				if(*topleft == 'x')
				{
					life++;
				}
				topleft++;
				if(*topmid == 'x')
				{
					life++;
				}
				topmid++;
				if(*topright == 'x')
				{
					life++;
				}
				topright++;
				if(*midleft == 'x')
				{
					life++;
				}
				midleft++;
				if(*midright == 'x')
				{
					life++;
				}
				midright++;
				if(*botleft == 'x')
				{
					life++;	
				}
				botleft++;
				if(*botmid == 'x')
				{
					life++;
				}
				botmid++;
				if(*botright == 'x')
				{
					life++;
				}
				botright++;

				//�berpr�fung auf Ver�nderung einer Lebendigen Zelle
				if(*center == 'x' && (life < 2 || life >= 4))
				{
					gboard[j][k] = '.';
				}
				
				//�berpr�fung auf Ver�nderung einer Toten Zelle
				else if(*center == '.' && life == 3)
				{
					gboard[j][k] = 'x';
				}
				center++;
				//Lebencounter wieder auf 0 setzen
				life=0;
			}
		}
	}

	//Zeitmessung f�r Haupt Algorithmus stoppen und ausgeben
	if(cmeasure == true)
	{
		ctimer.stop();
		ctimer.printTime(ctimer);
	}
}

	

 

