#include "Gol.h"

using namespace std; 

string s;

Gol::Gol(void)
{
	char zeichen = 0;
	int height = 0;
	int	width = 0;
	char** board = 0;
	Grid* m_Gridout = 0;
	Grid* m_Gridin = 0;
	bool gmeasure = false;	
	bool error = false;
}


Gol::~Gol(void)
{
	//2Dim Array l�schen

	for(int i=0;i<height;i++)
	{
			delete [] board[i];
	}
	delete [] board;	
	
}

void Gol::readfile(char* fname, int gen, bool measure)
{
	string sizew,sizeh;
	size_t pos = 0;
	string delimiter = ",";
	ifstream file (fname);
	gmeasure = measure;
	n_gen = gen;
	m_Gridout = new Grid();

	error = true;

	//Zeitmessung f�r Einlesen und Boarderstellung starten
	if(gmeasure==true)
		gtimer.start();

	if(file.is_open())
	{
		//Erste Zeile auslesen und trennen		
		getline(file,s);
		pos = s.find(delimiter);
		sizew = s.substr(0,pos);
		sizeh = s.substr(pos+1);

		//String casten auf Int
		width = atoi(sizew.c_str());
		height = atoi(sizeh.c_str());
	
		//Board dynamisch allokieren
		board = new char *[height];
		for(int i=0;i<height;i++)
		{
			board[i] = new char[width];
		}

		// Board erstellen befuellen
		for(int i=0; i<height; i++)
		{
			for(int j=0; j<width; j++)
			{
				file.get(r);
				if(r == '\n')
					file.get(r);
				board[i][j] = r;			
			}
		}

		if(gmeasure == true)
	{
		gtimer.stop();
		gtimer.printTime(gtimer);
	}
	
	error = false;

	//Eingelesenes Board an Grid mittels getboard methode �bergeben
	m_Gridout->getboard(height,width,n_gen, gmeasure, board);
	}	

	//Erstellen von board falls kein File vorhanden ist 

	if(error == true)
	{
		board = new char *[1];
		for(int i=0;i<1;i++)
		{
			board[i] = new char[1];
		}
	}	
}

void Gol::savefile(char* sname)
{
	ofstream file (sname);
	m_Gridin = new Grid();

	if(file.is_open() && error == false)
	{
		//Ver�ndertes Board von Grid mittels setboard methode �bernehmen
		m_Gridin->setboard(board);

		//Width und Height in erste Zeile einf�gen
		file << s;
	
		//Board Zeichen f�r Zeichen in ein File Speichern
		for(int i=0; i<height; i++)
		{
			w='\n';
			file.put(w);
			for(int j=0; j<width; j++)
			{
				w = board[i][j];
				file.put(w);						
			}
		}
	}

	//Testausgabe des fertigen Boards
	/*
	cout << "Hoehe:" << height << "Breite:" << width << endl;
	cout << "Eingelesenes Board:" << endl;
	for(int i=0; i<height; i++)
	{
		cout << endl;
		for(int j=0; j<width; j++)
		{
			cout << board[i][j];
		}
	}*/
}


